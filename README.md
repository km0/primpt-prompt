# Prompt primpt

A prompt primpter script for the [Draw It With Others](https://draw-it-with-others.org/) activities at the [H&D Summer Camp 2024](https://www.hackersanddesigners.nl/diwo-draw-it-with-others.html). Print Markdown documents on a receipt printer. Fork of Joak's [Extratonal Special Flyer](https://git.xpub.nl/joak/extratonalspecialflyer) script.

## Install

First clone the repo, create a virtual environment and activate it.

```bash
python3 -m venv venv
source venv/bin/activate
```

Install the dependencies referring to the `requirements.txt` file.

```bash
pip install -r requirements.txt
```

In case you get errors during the installation of `pycups`, try running:

```bash
# Debian
sudo apt-get install libcups2-dev
```

Then install again.

## Usage

### Prepare

Before launching the script for the first time, run:

```python
python prepare.py
```

It will prepare the prompts folder by cloning the connected repo.

### Print

First things first, would be nice to get a list of available prompts to primpts. In order to do that you can run:

```bash
$ python prompt.py --list

List of available prompts:

Bonfire live drawing
Xquisite Branch
Smartphone Camera Obscura
SVG-Swap
Connect the dots!
Line-Swap
Performing Pen Plotting
d-r-a-w-i-t-h-o-e
```

You can then choose one of these and print it by running:

```bash
$ python prompt.py --prompt "Connect the dots!"
```

With a bit of luck and fingers crossed, the printer should start buzzing and primpting the prompt!

### Where are the prompts?

This script works in tandem with a sister repository that takes care of contents. By default it is connected to [DIWO](https://gitlab.com/chae0e719/diwo-reuse), but you can change it by creating an `.env` file in the folder of your project and setting the `PROMPTS_REPO` variable.

For example:

```env
PROMPTS_REPO=https://gitlab.com/chae0e719/diwo-reuse
```

_Note that this setup is not strictly connected to the printer-render situation. It's just because we need a collaborative surface where to write together. This step can be bypassed easily simply creating a prompts folder as specified below._

Primpt prompt assumes that your repo is organized in this way:

```
prompts
├── your_prompt
│   └── README.md
│   └── img.jpg
├── another_prompt
│   └── README.md
└── README.md
```

The expected structure is really simple: a general `README.md` file, and a folder for each prompt. Each prompt folder holds a `README.md` (the name is case-sensitive!) file with the prompt written in Markdown following the [CommonMark specs](https://spec.commonmark.org/0.30/). Local images can be put in the same folder of the prompt and used from the document using simple relative paths

For example, given the structure above, this code should suffice for displaying the image, (given that said image is in the same folder of the README file):

```md
![An example image](img.jpg)
```

### Magic Words

It is possible to use _magic words_ in the markdown documents to activate interactive functions. One example is found in the "Connect the dots!" prompt: by using the magic word `|connect|`, the renderer call the `draw_dots` function to print the image of a random SVG drawing with a pointillisme attitude.

At the moment the structure is quite flexible: magic words can be added by adding a function to the `magic.py` module. The only requirement is to add the printer as parameter of the function.

**NOTE THAT MAGIC WORDS ARE CASE SENSITIVE!**

The syntax to call a magic word from a markdown file is:

```
|function|
```

arguments can be passed optionally following the sintax:

```
|function>arg, arg, arg|
```

they will be parsed as array of positional parameters

example for the magic word to generate a QR code for the DIWO website

```
|QR>https://draw-it-with-others.org/|
```

will return

```python
function="QR"
args=['https://draw-it-with-others.org/']
```

Here is an example:

```python
def QR(printer: Usb, *args):
    url = args[0]
    printer.qr(url)
    return url
```

To call the magic word from a markdown document write `|QR>https://draw-it-with-others.org/|`, the name of the function between "|".
The printer result will add the text "open sesame" to your contents.

This is just one example, look at the `magic.py` file for other ones!

### List of magic words

| Magic word | Params  | Description                                                             | Example                                   |
| ---------- | ------- | ----------------------------------------------------------------------- | ----------------------------------------- |
| connect    |         | Extract the dots from a random SVG in the images folder, then print it  | `\|connect\|`                             |
| greeting   |         | Print a random salutation. Find the list of available terms in magic.py | `\|greeting\|`                            |
| QR         | content | Generate and then print a QR code for the specified contents            | `\|QR>https://draw-it-with-others.org/\|` |
| sizes      |         | Test print with different sizes and smoothing options                   | `\|sizes\|`                               |

## Troubleshooting Debian WSL

We are using a EPSON TM-T20 80mm receipt printer. Here are some info about how we make it work.
Here's a guide to make it work with Debian running on a WSL setup.

Connect the printer with USB. Install [usbipd-win](https://github.com/dorssel/usbipd-win). Run `usbipd list` on a Windows commandline.

```powershell
$ usbipd list
Connected:
BUSID  VID:PID    DEVICE                             STATE
1-1    1235:800a  Scarlett 2i4 USB                   Not shared
1-3    04b8:0e03  EPSON TM-T20                       Shared (forced)
1-8    5986:9102  BisonCam,NB Pro                    Not shared
1-10   06cb:00c7  Synaptics UWP WBDI                 Not shared
1-14   8087:0029  Intel(R) Wireless Bluetooth(R)     Not shared
```

It should detect the printer. Note the `BUSID` value (should look something like `1-3`)

Run `usbipd attach --wsl --busid <BUSID>`, replacing `<BUSID>` with the bus id of your printer.

It should prompt something like:

```powershell
usbipd: info: Using WSL distribution 'Debian' to attach; the device will be available in all WSL 2 distributions.
usbipd: info: Using IP address 172.30.224.1 to reach the host.
```

Now you can log into Debian. If you run `lsusb` the printer should show up.

```
Bus 001 Device 007: ID 04b8:0e03 Seiko Epson Corp. Thermal Receipt Printer [TM-T20]
```

If it does, note the two hex numbers written after ID. They are the idVendor and idProduct values needed to identify the printer.

Then it's time to create a `udev` configuration file with some rules to grant permission to print to our user.

```
sudo nano /etc/udev/rules.d/10-local.rules
```

And in there write

```
SUBSYSTEMS=="usb", ENV{DEVTYPE}=="usb_device", ATTRS{idVendor}=="04b8", ATTRS{idProduct}=="0e03", GROUP="plugdev", MODE="0777"
```

Replacing `04b8` and `0e03` with your values. Be sure as well that your user is in the group `plugdev`.

After doing this, reload the udev service running:

```
sudo udevadm control --reload
sudo udevadm trigger
```

If udev is not running you will encounter some errors, such as `Failed to send reload request: No such file or directory`
In that case, just start the service first running

```
sudo service udev start
```

It will prompt a really intimidating message, but just ignore it and wait for 60 seconds for the script to run. Then try to reload udev again. And the printer should work........

For more info about `udev` see: [setup udev for usb printers](https://python-escpos.readthedocs.io/en/latest/user/installation.html#setup-udev-for-usb-printers)
