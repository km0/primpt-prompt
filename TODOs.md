-   Things to figure out:
    -   [x] Way to specify prompt to primpt --> using commandline arguments
    -   [x] How to customize render functions --> extending the marko markdown renderer!
    -   [x] Replace print statement with printer's print in prompt.py --> doing it directly in the renderer!
-   Rendering of:
    -   [x] Headers
    -   [x] Paragraphs
    -   [x] Images
    -   [x] Links
    -   [] Quotes
-   Document style guide for image names etc. (i.e: using whitespace in the filename break the parser!)
-   [x] Magic words system
