from svgpathtools import svg2paths, wsvg
from cairosvg import svg2png
from escpos.printer import Usb
from enum import Enum
from svg.path import parse_path


# Enum helper to select
class Mode(Enum):
    START = 1
    END = 2
    START_END = 3
    ALL = 4


def draw_dots(
    svg: str,
    mode=Mode.START_END,
    width=288 * 2,
    height=288 * 2,
    radius=4,
    stroke_width=2,
    trace_path=False,
    preview=False,
):

    # Parse the svg paths
    paths, _, attributes = svg2paths(svg, return_svg_attributes=True)

    svg_width = attributes["width"]
    svg_height = attributes["height"]

    # Scale size and keep ratio
    w_percent = width / float(svg_width)
    height = int((float(svg_height) * float(w_percent)))

    # Adjust radius on SVG total size
    scaled_radius = (float(svg_width) / width) * radius / 500

    # Extract the dots with the desired function
    match mode.name:
        case "START":
            dots = [path[0].start for path in paths]
        case "END":
            dots = [path[0].end for path in paths]
        case "START_END":
            dots = [
                dot
                for path in paths
                for dot in [path[0].start, path[len(path) - 1].end]
            ]
        case "ALL":
            dots = [dot.start for path in paths for dot in path]
            dots = [dots[index] for index in range(1, len(dots), 50)]

    # Define a temporary file where to store the svg
    temp = "images/temp.svg"

    # When printing without tracing the paths, just delete them from the list
    if not trace_path:
        paths = []

    # Generate the dots svg
    wsvg(
        filename=temp,
        paths=paths,
        stroke_widths=[stroke_width] * len(paths),
        nodes=dots,
        node_radii=[scaled_radius] * len(dots),
        node_colors=[(0, 0, 0)] * len(dots),
        openinbrowser=preview,
    )

    # Generate a filename for the resulting png
    # using the combination original-filename_mode-name
    filename = f"{svg.replace('.svg', '')}_{mode.name}.png"

    # save the resulting png
    # size is now by default the one used by the escpos printer
    svg2png(
        url=temp,
        write_to=filename,
        output_width=width,
        output_height=height,
    )

    # return the image filename
    return filename


if __name__ == "__main__":
    img = draw_dots("images/22724_110205_001.svg", mode=Mode.ALL)

    # printer = Usb(0x4B8, 0x0E03, profile="TM-T20II")
    # printer.image(img)
    # printer.cut()


# from each path we need the starting and ending point
#
# a list comprehension within a list comprehension......
# if this is the pythonic way of looking at the world......
# flat_list = [item for sublist in regular_list for item in sublist]
# dots = [dot for path in paths for dot in [first point, last point]]
#
# translated:
# make a list of dots where
# for each path in the list of paths
# take the starting point of the first segment `path[0].start`
# and the ending point of the last segment `path[len(path)-1].end`
#
# to be fair this feels more like doing sudoku than programming
# (funny in its own way)
# dots = [dot for path in paths for dot in [path[0].start, path[len(path) - 1].end]]
