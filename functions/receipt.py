from escpos.printer import Usb


def header(printer: Usb):
    printer.set(
        align="center",
        normal_textsize=True,
        invert=False,
        bold=False,
        underline=False,
    )
    printer.ln()
    printer.textln("PROMPT OF THE DAY")


def footer(printer: Usb):
    printer.ln(3)
    printer.set(font="a", underline=False)
    printer.textln("_.-." * 12)
    printer.ln(2)

    printer.set(align="center", font="b")
    printer.textln("To check other prompts visit")
    printer.set(underline=True, font="a")
    printer.textln("draw-it-with-others.org")
    printer.ln()

    printer.qr(
        "https://draw-it-with-others.org/",
        center=True,
        size=5,
    )

    printer.set(align="center", font="b", underline=False)
    printer.text(
        """This prompt is part of Draw-It-With-Others, 
a project initiated by the DIWO working group 
"""
    )

    # 🙄🤣
    printer.text("[Chae & Kamo] ( ")
    printer.set(underline=True)
    printer.text("chae0.org")
    printer.set(underline=False)
    printer.text(" & ")
    printer.set(underline=True)
    printer.text("kamomomomomomo.org")
    printer.set(underline=False)
    printer.text(" )")

    printer.textln(
        """
in the context of the Hackers & Designers 
Summer Camp 2024 Unruly Currents & Everyday Piracy.
"""
    )

    printer.cut()
