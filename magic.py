from escpos.printer import Usb
from functions.connect import draw_dots, Mode
from glob import glob
import random
import os


def greeting(printer: Usb):
    """Print a random salutation"""
    terms = [
        "Hello",
        "Good morning",
        "Bonjour",
        "Hola",
        "Ciao",
        "Salut ça va bonzur",
        "Yoooooo",
        "Hei",
        "Eeeeeeeehhh ''''ssssuuuuupp",
        "Oi oi oi",
        "Brrrrrrrrrrrrrrrrrrrr",
    ]
    printer.text(random.choice(terms))


def QR(printer: Usb, *args):
    """
    Print QR code of the arg content
    syntax:
    |QR>content|
    """
    content = args[0]
    printer.ln()
    printer.qr(content, center=True, size=5)
    printer.set(align="center", underline=True, font="b")
    printer.textln(content)

    return ""


def connect(printer: Usb):
    """
    Extract the dots from a random SVG in the images folder, then print it.
    """
    # TODO: print also source drawing url...
    # that means: we need to put source drawing somewhere
    # such as in a static folder on draw-it-with-others.org ?
    # can we upload them programmatically from here ?
    # that would save soo much trouble
    files = glob("images/*.svg")
    svg = random.choice(files)
    img = draw_dots(svg, mode=Mode.ALL)
    printer.image(img)
    printer.ln()
    printer.ln()
    printer.set(
        font="a",
        align="left",
        bold=True,
        invert=True,
        normal_textsize=True,
        underline=False,
    )
    printer.textln("STEPS")
    printer.ln()
    printer.set(
        font="a",
        align="left",
        bold=False,
        invert=False,
        normal_textsize=True,
        underline=False,
    )
    printer.textln("1. Connect the dots")
    printer.textln("2. (OPTIONAL) Check the source drawing")
    QR(printer, f"https://draw-it-with-others.org/drw-radio/{os.path.basename(svg)}")
    return ""


def sizes(printer: Usb):
    """
    Test available sizes in the printer

    Args:
        printer (Usb): Usb printer initialized in the printer renderer
    """

    printer.set(normal_textsize=True)
    printer.ln()
    printer.textln("Test sizes, non smooth")

    for i in range(8):
        printer.set(custom_size=True, width=i + 1, height=i + 1, smooth=False)
        printer.text(f"{i+1}")

    printer.set(normal_textsize=True)
    printer.ln()
    printer.textln("Test sizes, super smooth")

    for i in range(8):
        printer.set(custom_size=True, width=i + 1, height=i + 1, smooth=True)
        printer.text(f"{i+1}")

    printer.set(normal_textsize=True)
    printer.ln()
    printer.textln("Test density")

    for i in range(8):
        printer.set(density=i + 1)
        printer.text(f"{i+1}")
    printer.set(normal_textsize=True)
    printer.ln()
    printer.textln("Test underline")

    for i in range(3):
        printer.set(custom_size=True, underline=i, width=2, height=2, smooth=True)
        printer.textln(f"Underline {i}")
