import os
import shutil
import subprocess
from dotenv import load_dotenv


def clone():
    """
    Clone the repository defined in the environmental variable PROMPTS_REPO.
    Default set to: https://gitlab.com/chae0e719/diwo-reuse
    """
    load_dotenv()
    repo = os.environ.get("PROMPTS_REPO", "https://gitlab.com/chae0e719/diwo-reuse")
    print(repo)

    if os.path.isdir("prompts"):
        try:
            shutil.rmtree("prompts")
        except OSError as e:
            print(e)

    subprocess.run(["git", "clone", repo, "prompts"])


def pull(debug=False):
    """
    Pull from the repo cloned during the initialization.
    """
    try:
        subprocess.run(
            ["git", "pull"],
            cwd="prompts",
            stdout=subprocess.DEVNULL if debug else subprocess.PIPE,
        )
    except OSError as e:
        print(e)


if __name__ == "__main__":
    clone()
