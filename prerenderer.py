from marko.helpers import MarkoExtension
from marko.inline import Image, Link
from marko.block import Heading, Paragraph, Quote
from marko.md_renderer import MarkdownRenderer
from marko import inline

# Define Magic Word element as way to call dynamic functions from markdown files
# for now using the simple format |magic| - where magic is the name of the function to run
# look at the magic words file for more!


class MagicWord(inline.InlineElement):

    # Pattern for magic word is:
    # |function|
    #
    # arguments can be passed optionally following the sintax:
    # |function>arg, arg, arg|
    #
    # they will be parsed as array of positional parameters
    #
    # example for the magic word to generate a QR code for the DIWO website
    # |QR>https://draw-it-with-others.org/|
    # will return
    # function="QR"
    # args=['https://draw-it-with-others.org/']

    pattern = r"\|([^\|]*)\|"
    parse_children = True
    function = ""
    args = []

    def __init__(self, match):
        func, _, args = match.group(1).partition(">")
        self.function = func
        if args != "":
            self.args = [arg.strip() for arg in args.split(",")]


class MagicWordRendererMixin(object):
    def render_magic_word(self, element):
        return element.function


MagicWords = MarkoExtension(
    elements=[MagicWord], renderer_mixins=[MagicWordRendererMixin]
)


class ReceiptRenderer(MarkdownRenderer):
    """
    Render AST for printing on Receipt Printer.
    Based on MarkdownRenderer with few variations.
    """

    def __init__(self) -> None:
        super().__init__()

    def render_heading(self, element: Heading) -> str:
        result = (
            "\n"
            + '{{ set, {"font":"a", "align": "left", "bold": True, "invert": True, "normal_textsize": True, "underline": False} }}'
            + (
                '{{ set, {"font":"a", "align": "center", "bold": True, "invert": True, "double_width": True, "double_height": True} }}'
                if element.level == 1
                else ""
            )
            + f" {self.render_children(element)} \n"
            + ("\n" if element.level == 1 else "")
        )

        return result

    def render_paragraph(self, element: Paragraph) -> str:
        prerender = '{{ set, {"font": "a", "align": "left", "bold": False, "invert":False, "normal_textsize": True, "underline":False} }}' + super().render_paragraph(
            element
        )
        return prerender

    def render_image(self, element: Image) -> str:
        src = element.dest.replace('"', "").replace("/", "")
        result = (
            "{{ img, " + str({"src": src, "alt": self.render_children(element)}) + " }}"
        )
        return result

    def render_link(self, element: Link) -> str:
        result = (
            '{{ set, {"font": "b"} }}'
            + self.render_children(element)
            + "\n"
            + '{{ set, {"underline": True} }}'
            + element.dest
            + '{{ set, {"font": "a", "underline": False} }}'
            + "\n"
        )
        return result

    def render_magic_word(self, element: MagicWord):
        return (
            "{{magic, "
            + str(
                {
                    "function": element.function,
                    "args": element.args if element.args else [],
                }
            )
            + " }}"
        )

    def render_emphasis(self, element: inline.Emphasis) -> str:
        return (
            ""
            + '{{ set, {"underline": 2} }}'
            + self.render_children(element)
            + '{{ set, {"underline": 0} }}'
        )

    def render_strong_emphasis(self, element: inline.StrongEmphasis) -> str:
        return (
            ""
            + '{{ set, {"bold": True} }}'
            + self.render_children(element)
            + '{{ set, {"bold": False} }}'
        )

    def render_quote(self, element: Quote) -> str:
        return (
            ""
            + '{{ set, {"font": "b"} }}'
            + self.render_children(element).rstrip("\n")
            + '{{ set, {"font": "a"} }}'
            + "{{ln}}"
        )
