import prepare
from marko import Markdown, ast_renderer
from prerenderer import ReceiptRenderer, MagicWords
import argparse
from escpos.printer import Usb
from glob import glob
from functions.receipt import header, footer
from renderer import PrinterRenderer


parser = argparse.ArgumentParser(
    prog="Primt Prompt",
    description="A utility to primpt Markdown documents on receipt primpter",
    epilog="If it doesn't work I'm sorry!",
)

parser.add_argument(
    "-p",
    "--prompt",
    help="Which prompt to primpt, choose from the connected repository.",
    required=False,
)

parser.add_argument(
    "-nf",
    "--nofoot",
    help="Primpt the prompt without footer",
    action="store_true",
    required=False,
)


parser.add_argument(
    "-l", "--list", action="store_true", help="Print a list of the available prompts"
)

parser.add_argument(
    "-d",
    "--debug",
    action="store_true",
    help="In debug mode use the terminal instead of the printer as output for printing. Saves km of paper.",
)

printer = Usb(0x4B8, 0x0E03, profile="TM-T20II")


def primpt(prompt):
    markdown = Markdown(extensions=[MagicWords])

    if args.debug:
        renderer = ast_renderer.ASTRenderer()
    else:
        renderer = ReceiptRenderer()

    with open(f"prompts/{prompt}/README.md") as f:
        document = markdown.parse(f.read())
        if args.debug:
            print(renderer.render(document))
        else:
            header(printer)
            prerender = renderer.render(document)
            p = PrinterRenderer(
                prompt,
                printer,
            )
            p.render(prerender)
            if not args.nofoot:
                footer(printer)
            else:
                printer.cut()


if __name__ == "__main__":
    args = parser.parse_args()
    # prepare.pull(args.debug)
    try:
        if args.list:
            prompts = [
                prompt.partition("/")[2].partition("/")[0]
                for prompt in glob("prompts/*/README.md")
            ]
            print("\nList of available prompts:\n")
            for prompt in prompts:
                print(prompt)
        if args.prompt:
            primpt(args.prompt)
    except Exception as e:
        print(e)
