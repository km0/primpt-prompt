import re
import ast
import renderer
from escpos.printer import Usb
from PIL import Image
import magic


class PrinterRenderer:
    """
    Render a string file prepared with the ReceiptRenderer to a Usb printer
    Initialize with a path that works as baseurl for the file linked in the markdown &&&
    with a Usb printer : )

    """

    printer: Usb
    cwd = ""

    def __init__(self, cwd: str, printer: Usb) -> None:
        self.printer = printer
        self.cwd = cwd

    def ln(self):
        self.printer.ln()

    def set(self, **args):
        self.printer.set(**args)

    def text(self, content):
        self.printer.text(content)

    def img(self, **args):

        # Rescale image to full width
        full_width = 288 * 2
        img = Image.open(f"prompts/{self.cwd}/{args['src']}")
        w_percent = full_width / float(img.size[0])
        h_size = int((float(img.size[1]) * float(w_percent)))
        img = img.resize((full_width, h_size), Image.Resampling.LANCZOS)

        self.printer.image(img)
        self.printer.ln()
        self.printer.set(font='b', align="center")
        self.printer.textln(args["alt"])

    def magic(self, **args):
        try:
            magic_word = getattr(magic, args["function"])
            return magic_word(self.printer, *args["args"])
        except Exception:
            return args["function"]

    def render(self, document):
        """
        Iterate through the document parsing one print command at the time
        """

        cursor = 0
        commands = re.finditer(r"\{\{(.+?)\}\}", document)

        for c in commands:
            # Print previous iteration contents
            if c.start() > cursor:
                content = document[cursor : c.start()]
                self.text(content)
            cursor = c.end()

            try:
                command, _, args = c.group(1).partition(",")
                command = command.strip()
                args = ast.literal_eval(args) if args != "" else {}
                func = getattr(renderer.PrinterRenderer, command)
                func(self, **args)
            except Exception as e:
                print(f"\n{e}\n{args}\n")
